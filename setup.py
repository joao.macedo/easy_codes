#!/usr/bin/env python
#-*- coding:utf-8 -*-

# MIT License
# 
# Copyright (c) 2018 João Macedo
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import io
from os.path import basename
from os.path import dirname
from os.path import join

from setuptools import setup, find_packages

def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf-8")
    ).read()

setup(
    name = "easy_codes",
    version = read("VERSION"),
    description = (
        "A python module for reading (ECMWF) GRIB files. "
    ),
    long_description = read("README.md"),
    author = "João Macedo",
    author_email = "joao.macedo@likeno.pt",
    url = "https://gitlab.com/joao.macedo/easy_codes.git",
    classifiers = [""],
    platforms = [""],
    license = "MIT license",
    packages = find_packages("src"),
    package_dir = {'': "src"},
    install_requires = [
        "eccodes-python",
    ]
)

