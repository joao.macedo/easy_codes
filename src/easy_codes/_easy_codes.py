#! /usr/bin/env python
#-*- coding:utf-8 -*-


"""ecCodes Installation: https://software.ecmwf.int/wiki/display/ECC/ecCodes+installation
ecCodes Releases: https://software.ecmwf.int/wiki/display/ECC/Releases
"""
import eccodes
import io
import numpy as np
from collections import OrderedDict
from itertools import product

    


# GRIB vs BUFR functions mapping
EC_FUNC_DICT = {
    'grib': {'new_from_file': eccodes.codes_grib_new_from_file,
             'index_new_from_file': eccodes.codes_index_new_from_file,
             'keys_iterator_get_name': eccodes.codes_keys_iterator_get_name,
             'keys_iterator_next':eccodes.codes_keys_iterator_next,
             'keys_iterator_delete': eccodes.codes_keys_iterator_delete},
    'bufr': {'new_from_file': eccodes.codes_bufr_new_from_file,
             'index_new_from_file': eccodes.codes_bufr_new_from_file,
             'keys_iterator_get_name':eccodes.codes_bufr_keys_iterator_get_name,
             'keys_iterator_next': eccodes.codes_bufr_keys_iterator_next,
             'keys_iterator_delete': eccodes.codes_bufr_keys_iterator_delete}
}

def keys_iterator_new(var_id, namespace='', fmt='grib'):
    """Workarround to suport namespace argument for grib format...
    """
    if fmt == 'grib':
        return eccodes.codes_keys_iterator_new(var_id, namespace=namespace)
    elif fmt == 'bufr':
        return eccodes.codes_bufr_keys_iterator_new(var_id)
 

def get_selected_variables(fname, selection_dict, kw_list=[],
                           namespace='', only_metadata=False):
    """Get selected variables (data and metadata).

    selection_dict: Use a python dictionary with the metadata keywords and
        the respective value or list of values to create a selection filter.
        Use the 'get_available_keywords_values' to retrieve all the available
        keywords and respective possible values that can be used in the
        selection_dict filter.

    kw_list: A list of the metadata keywords to be retrieved from the
        selected variables.
        If an empty list is given, all the metadata will be retrieved.

    namespace: The 'namespace' argument can be used to retrieve all metadata
        from a specific namespace. This option only can be used if the
        'kw_list' is an empty list. 
        Available namespaces: 'parameter', 'time', 'geography', 'vertical'

    returns: A list of python dictionaries with the 'data' and 'metadata'
         keys, each one.
             * 'metadata': dictionary with the variable's metadata
             * 'data': numpy array with the variable's data

    (Alternatively you can use 'get_selected_data' or 'get_selected_metadata'
     to retrieve, only, the selected data or metadata respectively)

    exemple:
    ---------
    Retrieve 10u and 10v variables data and metadata specified in
    'kw_list' for the model time step 12 (sec):

    >>> fname = "GRIB_F-012_ECMWF_All_Globe_201812040000"
    >>> meta_kw_list = [
        "Nx", "Ny", "latitudeOfFirstGridPointInDegrees",
        "latitudeOfLastGridPointInDegrees", "longitudeOfFirstGridPointInDegrees",
        "longitudeOfLastGridPointInDegrees", "iDirectionIncrementInDegrees",
        "jDirectionIncrementInDegrees", "shortName", "step"
    ]
    >>> easy_codes.get_selected_variables(fname, {'shortName':['10u','10v'], 'step':12},
                                            meta_kw_list)
    [{'data': array([[0.638, 0.650, 0.663, ..., 0.602, 0.614, 0.626],
             ...,
             [1.742, 1.748, 1.754, ..., 1.719, 1.727, 1.735]]),
      'metadata': {'Nx': 2880, 'Ny': 1441,
       'iDirectionIncrementInDegrees': 0.125, 'jDirectionIncrementInDegrees': 0.125,
       'latitudeOfFirstGridPointInDegrees': 90.0, 'latitudeOfLastGridPointInDegrees': -90.0,
       'longitudeOfFirstGridPointInDegrees': -180.0, 'longitudeOfLastGridPointInDegrees': 179.875,
       'shortName': '10v', 'step': 12}},
     {'data': array([[-5.693, -5.690, -5.687, ..., -5.692, -5.692, -5.693],
             ...,
             [ 3.225,  3.219 ,  3.214, ...,  3.233, 3.230,  3.227 ]]),
      'metadata': {'Nx': 2880, 'Ny': 1441,
       'iDirectionIncrementInDegrees': 0.125, 'jDirectionIncrementInDegrees': 0.125,
       'latitudeOfFirstGridPointInDegrees': 90.0, 'latitudeOfLastGridPointInDegrees': -90.0,
       'longitudeOfFirstGridPointInDegrees': -180.0, 'longitudeOfLastGridPointInDegrees': 179.875,
       'shortName': '10u', 'step': 12}}]

    """
    result = []
    for var_id in select_variables(fname, selection_dict):
        var_dict = {}
        if not only_metadata:
            var_dict['data'] = get_data_from_variable(var_id)
        if any(kw_list):
            var_dict['metadata'] = get_metadata_from_variable(var_id, kw_list)
        else: 
            var_dict['metadata'] = get_metadata_from_variable(
                var_id, namespace=namespace
            )
        result.append(var_dict)
        eccodes.codes_release(var_id)
    return result


def get_selected_data(fname, selection_dict):
    """Returns a list of numpy arrays with the selected variables data.

    selection_dict: Use a python dictionary with the metadata keywords and
        the respective value or list of values to create a selection filter.
        Use the 'get_available_keywords_values' to retrieve all the available
        keywords and respective possible values that can be used in the
        selection_dict filter.

    exemple:
    ---------
    Retrieve 10u and 10v variables data for the model time step 12 (sec):

    >>> fname = "GRIB_F-012_ECMWF_All_Globe_201812040000"
    >>> easy_codes.get_selected_data(fname, {"shortName":['10u','10v'], 'step':12})
    [array([[0.638, 0.650, 0.663, ..., 0.602, 0.614, 0.626],
            ...,
            [1.742, 1.748, 1.754, ..., 1.719, 1.727, 1.735]]),
     array([[-5.693, -5.690, -5.687, ..., -5.692, -5.692, -5.693],
            ...,
            [ 3.225,  3.219 ,  3.214, ...,  3.233, 3.230,  3.227 ]])]

    """
    result = []
    for var_id in select_variables(fname, selection_dict):
        result.append(get_data_from_variable(var_id))
        eccodes.codes_release(var_id)
    return result


def get_selected_metadata(fname, selection_dict, kw_list=[],
                          namespace=''):
    """Returns a list of python dictionaries with the selected variables 
       metadata.

    selection_dict: Use a python dictionary with the metadata keywords and
        the respective value or list of values to create a selection filter.
        Use the 'get_available_keywords_values' to retrieve all the available
        keywords and respective possible values that can be used in the
        selection_dict filter.

    kw_list: A list of the metadata keywords to be retrieved from the
        selected variables.
        If an empty list is given, all the metadata will be retrieved.

    namespace: The 'namespace' argument can be used to retrieve all metadata
        from a specific namespace. This option only can be used if the
        'kw_list' is an empty list. 
        Available namespaces: 'parameter', 'time', 'geography', 'vertical'

    exemple:
    ---------
    Retrieve 10u and 10v variables metadata specified in
    the 'parameter' namespace, for the model time step 12 (sec):

    >>> fname = "GRIB_F-012_ECMWF_All_Globe_201812040000"
    >>> easy_codes.get_selected_metadata(fname, {"shortName":['10u','10v'], 'step':12},
                                         namespace='parameter')
    [{'centre': 'ecmf', 'name': '10 metre V wind component',
       'paramId': 166, 'shortName': '10v', 'units': 'm s**-1'},
     {'centre': 'ecmf', 'name': '10 metre U wind component',
       'paramId': 165, 'shortName': '10u', 'units': 'm s**-1'}]


    """
    vars_list = get_selected_variables(fname, selection_dict, kw_list,
                                       namespace, only_metadata=True)
    return [var['metadata'] for var in vars_list]


def select_variables(fname, selection_dict, fmt="grib"):
    """Retrieve a list of variable ids, filtered by the given selection_dict.  

    selection_dict: Use a python dictionary with the metadata keywords and
        the respective value or list of values to create a selection filter.
        Use the 'get_available_keywords_values' to retrieve all the available
        keywords and respective possible values that can be used in the
        selection_dict filter.

    NOTE: Call the eccodes.codes_release(var_id) function after use the
          respective variable or call free_selected_variables(var_id_list),
          with the returned variable ids list.

    exemples:
    ---------
    Retrieve a list of variable ids for the u10 variable for
    the model time step 0 (sec) and pressure levels 500 and 850 (hPa):

    >>> fname = "GRIB_F-012_ECMWF_All_Globe_201812040000"
    >>> var_list = easy_codes.select_variables(fname, {'shortName':'10u', 'step':0, 'level':[500, 850]})
    >>> for var_id in var_list:
    >>>     "do something with the selected variable..."
    >>> easy_codes.free_selected_variables(var_list)
    """
    result = []
    grib_id = EC_FUNC_DICT[fmt]['index_new_from_file'](
        fname, tuple(selection_dict.keys())
    )
    # Each selection filter may only take one value per keyword.
    # The itertools.product its being used for that purpose.
    selection_filters = product(*[
        values if isinstance(values, list) else [values]
        for values in selection_dict.values()
    ])
    for filter_values in selection_filters:
        for key, value in zip(selection_dict.keys(), filter_values):
            eccodes.codes_index_select(grib_id, key, value)
        while 1:
            var_id = eccodes.codes_new_from_index(grib_id)
            if var_id is None:
                break
            result.append(var_id)
    eccodes.codes_index_release(grib_id)
    return result


def free_selected_variables(var_id_list):
    """Free variable ids list returned by select_bariables. 
    """
    for var_id in var_id_list:
        eccodes.codes_release(var_id)


def get_metadata_from_variable(var_id, kw_list=[], namespace='', fmt="grib"):
    """Get metadata from variable with the given var_id.

    var_id: Index of the variable. Use the 'select_variables' function
        to retrieve a list of valid var_ids.

    kw_list: A list of the metadata keywords to be retrieved from the
        selected variables.
        If an empty list is given, all the metadata will be retrieved.

    namespace: The 'namespace' argument can be used to retrieve
        all metadata from a specific namespace. This option only can be used
        if the 'kw_list' is an empty list. 
        Available namespaces: 'parameter', 'time', 'geography', 'vertical'

    example:
    --------
    >>> fname = "GRIB_F-012_ECMWF_All_Globe_201812040000"
    >>> var_list = easy_codes.select_variables(fname, {"shortName":["2t","2d","tco3","tcwv","msl"]})
    >>> for var_id in var_list:
    >>>    metadata = easy_codes.get_metadata_from_variable(var_id, ["shortName", "paramId", "time", "step", "Nx", "Ny"])
    >>>    print(metadata)

    {'Nx': 2880, 'Ny': 1441, 'step': 12, 'time': 0, 'shortName': '2t', 'paramId': 167}
    {'Nx': 2880, 'Ny': 1441, 'step': 12, 'time': 0, 'shortName': '2d', 'paramId': 168}
    {'Nx': 2880, 'Ny': 1441, 'step': 12, 'time': 0, 'shortName': 'tco3', 'paramId': 206}
    {'Nx': 2880, 'Ny': 1441, 'step': 12, 'time': 0, 'shortName': 'tcwv', 'paramId': 137}
    {'Nx': 2880, 'Ny': 1441, 'step': 12, 'time': 0, 'shortName': 'msl', 'paramId': 151}
    >>> easy_codes.free_selected_variables(var_list)
    """
    items = {}
    if not any(kw_list):
        key_it = keys_iterator_new(var_id, namespace, fmt)
        while EC_FUNC_DICT[fmt]['keys_iterator_next'](key_it):
            key = EC_FUNC_DICT[fmt]['keys_iterator_get_name'](key_it)
            items[key] = _codes_get(var_id, key)
        EC_FUNC_DICT[fmt]['keys_iterator_delete'](key_it)
    else:
        for key in kw_list:
            items[key] = _codes_get(var_id, key)
    return items


def get_data_from_variable(var_id):
    """Get data from variable with the given var_id.

    var_id: Index of the variable. Use the 'select_variables' function
        to retrieve a list of valid var_ids.

    example:
    --------
    >>> fname = "GRIB_F-012_ECMWF_All_Globe_201812040000"
    >>> var_list = easy_codes.select_variables(fname, {"shortName":["2t","2d","tco3","tcwv","msl"]})
    >>> for var_id in var_list:
    >>>    data = easy_codes.get_data_from_variable(var_id)
    >>>    print(data)

    [[249.827 249.827 249.827 ... 249.827 249.827 249.827]
     ...
     [246.262 246.262 246.262 ... 246.262 246.262 246.262]]
    [[246.543 246.543 246.543 ... 246.543 246.543 246.543]
     ...
     [243.147 243.147 243.147 ... 243.147 243.147 243.147]]
    [[0.007 0.007 0.007 ... 0.007 0.007 0.007]
     ...
     [0.005 0.005 0.005 ... 0.005 0.005 0.005]]
    [[1.540 1.540 1.540 ... 1.540 1.540 1.540]
     ...
     [0.939 0.939 0.939 ... 0.939 0.939 0.939]]
    [[101652.375 101652.375 101652.375 ... 101652.375 101652.375 101652.375]
     ...
     [100294.875 100294.875 100294.875 ... 100294.875 100294.875 100294.875]]
    >>> easy_codes.free_selected_variables(var_list)

    """
    try:
        dims_dict = get_metadata_from_variable(var_id, ['Nx','Ny'])
        if all(dims_dict.values()):
            values = eccodes.codes_get_values(var_id).reshape(
                (dims_dict['Ny'], dims_dict['Nx'])
            )
        else:
            values = eccodes.codes_get_values(var_id)
    except tuple(eccodes.ERROR_MAP.values()):
        values = None
    return values


def get_values_from_all_variables(fname,
                                  keywords=['shortName', 'name',
                                            'units', 'dataType', 'step',
                                            'level', 'typeOfLevel'], sort=True):
    keywords_values = get_available_keywords_values(fname, sort=sort)
    metadata = get_selected_metadata(
        fname, {'shortName': keywords_values['shortName']},
        keywords
    )
    values = {}
    for m in metadata:
        var = values.setdefault(m['shortName'], {})
        for k, v  in m.items():
            if k != 'shortName':
                vlist = var.setdefault(k, [])
                if v not in vlist and v is not None:
                    vlist.append(v)
                    if sort:
                        try:
                            vlist.sort()
                        except TypeError:
                            vlist = (
                                sorted([el for el in vlist
                                        if not isinstance(el, str)]) +
                                [el for el in vlist
                                 if isinstance(el, str).sort()]
                            )
    
    return values


def print_values_from_all_variables(fname,
                                    keywords=['shortName', 'name',
                                              'units', 'dataType', 'step',
                                              'level', 'typeOfLevel']):
    values = get_values_from_all_variables(fname, keywords)
    for k, v in values.items():
        print("{}:".format(k))
        for kk,vv in v.items():
            print("\t{}: {}".format(kk,vv))


def get_available_keywords_values(fname, namespace='', fmt="grib", sort=True):
    """Returns a python dictionary with all the available keywords
    and the respective possible values.
    Use the available keywords and respective values to form selection
    flters.

    namespace: The 'namespace' argument can be used to retrieve
        metadata from a specific namespace.

    Available namespaces and respective attributes description are:

    'parameter':
    ------------
        centre - Originating generating centre
        paramId - Unique identifier of the parameter.
            A parameter in different formats (grib1/grib2)
            is identified by the same ID.
        shortName - Not unique. Different parameters can have the
            same short name.
        units - Units
        name - Not unique. Different parameters can have the same name. 

    'time':
    -------

        dataDate - Reference date
        endStep - End of the forecast interval or value of the forecast step
        startStep - Beginning of the forecast interval or value of the forecast
            step
        stepRange - startStep-endStep
        stepUnits - Units of the step. Accepted values:
            s->Second; m->Minute; h->Hour; 3h->3hours; 6h->6hours;
            12h->12hours; D->Day; M->Month; Y->Year; 10Y->Decade
            30Y->Normal(30years); C->Century
        
        dataTime - Reference time
        validityDate - Date of validity of the forecast
        validityTime - Time of validity of the forecast
        stepType - Any of the following:
            instant; avg; accum; max; min; diff; rms; sd; cov; ratio

    'geography':
    ------------

        space_view - Space view
        albers - Albers
        polar_stereographic - Polar stereographic
        lambert - Lambert
        mercator - Mercator
        stretched_rotated_sh - Stretched and rotated spherical harmonic
            coefficients
        stretched_sh - Stretched spherical harmonic coefficients
        sh - Spherical harmonic coefficients
        rotated_sh - Rotated spherical harmonic coefficients
        stretched_rotated_gg - Stretched and rotated gaussian
            latitude / longitude
        stretched_gg - Stretched gaussian latitude / longitude
        rotated_gg - Rotated gaussian latitude / longitude
        reduced_gg - Reduced gaussian latitude / longitude
        regular_gg - Regular gaussian latitude / longitude
        stretched_rotated_ll - Stretched and rotated latitude / longitude
        stretched_ll - Stretched latitude / longitude.
        rotated_ll - Rotated latitude / longitude rotated
        reduced_ll - Reduced latitude / longitude. The number of parallels is
            fixed.
            The number of points per parallel must be given for each parallel.
        regular_ll - Regular latitude / longitude

    'vertical':
    -----------

        bottomLevel - Bottom level
        level - Level
        pv - List of coefficients of the vertical coordinate
        topLevel - Top level
        typeOfLevel - Type of level. Accepted values:
            surface; cloudBase; cloudTop; isothermZero; adiabaticCondensation
            maxWind; tropopause; nominalTop; seaBottom; isothermal;
            isobaricInhPa; isobaricInPa; isobaricLayer; meanSea;
            heightAboveSea; heightAboveSeaLayer; heightAboveGround;
            heightAboveGroundLayer; sigma; sigmaLayer; hybrid;
            hybridLayer; depthBelowLand; depthBelowLandLayer; theta; thetaLayer
            pressureFromGround; pressureFromGroundLayer; potentialVorticity;
            eta; depthBelowSea; entireAtmosphere; entireOcean
    """
    selections_dict = {}
    with io.open(fname, 'r') as fid:
        while 1:
            fid_it = EC_FUNC_DICT[fmt]['index_new_from_file'](fid)
            if fid_it is None:
                break
            key_it = keys_iterator_new(fid_it, namespace, fmt)
            while EC_FUNC_DICT[fmt]['keys_iterator_next'](key_it):
                key = EC_FUNC_DICT[fmt]['keys_iterator_get_name'](key_it)
                keyword = selections_dict.setdefault(key, [])
                val = _codes_get(fid_it, key)
                if val not in keyword and val is not None:
                    keyword.append(val)
                    if sort:
                        try:
                            keyword.sort()
                        except TypeError:
                            keyword = (
                                sorted([el for el in keyword
                                        if not isinstance(el, str)]) +
                                [el for el in keyword
                                 if isinstance(el, str)]
                            )
            EC_FUNC_DICT[fmt]['keys_iterator_delete'](key_it)
            eccodes.codes_release(fid_it)
    return selections_dict


def print_available_keywords_values(fname, namespace='', sort=True):
    """Prints the available keywords and respective possible values
    in a readable format.

    namespace: The 'namespace' argument can be used to retrieve
        metadata from a specific namespace.

    Available namespaces and respective attributes description are:

    'parameter':
    ------------
        centre - Originating generating centre
        paramId - Unique identifier of the parameter.
            A parameter in different formats (grib1/grib2)
            is identified by the same ID.
        shortName - Not unique. Different parameters can have the
            same short name.
        units - Units
        name - Not unique. Different parameters can have the same name. 

    'time':
    -------

        dataDate - Reference date
        endStep - End of the forecast interval or value of the forecast step
        startStep - Beginning of the forecast interval or value of the forecast
            step
        stepRange - startStep-endStep
        stepUnits - Units of the step. Accepted values:
            s->Second; m->Minute; h->Hour; 3h->3hours; 6h->6hours;
            12h->12hours; D->Day; M->Month; Y->Year; 10Y->Decade
            30Y->Normal(30years); C->Century
        
        dataTime - Reference time
        validityDate - Date of validity of the forecast
        validityTime - Time of validity of the forecast
        stepType - Any of the following:
            instant; avg; accum; max; min; diff; rms; sd; cov; ratio

    'geography':
    ------------

        space_view - Space view
        albers - Albers
        polar_stereographic - Polar stereographic
        lambert - Lambert
        mercator - Mercator
        stretched_rotated_sh - Stretched and rotated spherical harmonic
            coefficients
        stretched_sh - Stretched spherical harmonic coefficients
        sh - Spherical harmonic coefficients
        rotated_sh - Rotated spherical harmonic coefficients
        stretched_rotated_gg - Stretched and rotated gaussian
            latitude / longitude
        stretched_gg - Stretched gaussian latitude / longitude
        rotated_gg - Rotated gaussian latitude / longitude
        reduced_gg - Reduced gaussian latitude / longitude
        regular_gg - Regular gaussian latitude / longitude
        stretched_rotated_ll - Stretched and rotated latitude / longitude
        stretched_ll - Stretched latitude / longitude.
        rotated_ll - Rotated latitude / longitude rotated
        reduced_ll - Reduced latitude / longitude. The number of parallels is
            fixed.
            The number of points per parallel must be given for each parallel.
        regular_ll - Regular latitude / longitude

    'vertical':
    -----------

        bottomLevel - Bottom level
        level - Level
        pv - List of coefficients of the vertical coordinate
        topLevel - Top level
        typeOfLevel - Type of level. Accepted values:
            surface; cloudBase; cloudTop; isothermZero; adiabaticCondensation
            maxWind; tropopause; nominalTop; seaBottom; isothermal;
            isobaricInhPa; isobaricInPa; isobaricLayer; meanSea;
            heightAboveSea; heightAboveSeaLayer; heightAboveGround;
            heightAboveGroundLayer; sigma; sigmaLayer; hybrid;
            hybridLayer; depthBelowLand; depthBelowLandLayer; theta; thetaLayer
            pressureFromGround; pressureFromGroundLayer; potentialVorticity;
            eta; depthBelowSea; entireAtmosphere; entireOcean
    """
    selections_dict = get_available_keywords_values(fname, namespace=namespace,
                                                    sort=sort)
    for key, values in selections_dict.items():
        print("{}: {}".format(key, values))


def get_available_values(fname, kw_list, fmt="grib", sort=True):
    """Get all possible values for each keyword of the given keywords list.
    The returned values can be used to create slection filters.
    Use the 'get_available_keywords_values' to retrieve all the available keywords
    that can be used.

    kw_list: A list of the metadata keywords which the available values will be
    retrieved.

    exemple: 
    >>> get_available_values(fname, ["shortName", "paramId", "level", "time", "step"])
    {'level': [0, 7, 28, 100],
     'paramId': [39, 40, 41, 42, 134, 137, 139, 151, 164,
      165, 166, 167, 168, 170, 183, 206, 235, 236],
     'shortName': ['10u', '10v', '2d', '2t', 'msl', 'skt',
      'sp', 'stl1', 'stl2', 'stl3', 'stl4', 'swvl1', 'swvl2',
      'swvl3', 'swvl4', 'tcc', 'tco3', 'tcwv'],
     'step': [12, 13, 14, 15, 16, 17, 18, 19 ,20, 21, 22, 23, 24],
     'time': [0]}
    """
    selections_dict = {}
    with io.open(fname, 'r') as fid:
        while 1:
            grib_it = EC_FUNC_DICT[fmt]['new_from_file'](fid)
            if grib_it is None:
                break
            for key in kw_list:
                keyword = selections_dict.setdefault(key, [])
                val = _codes_get(grib_it, key)
                if val not in keyword and val is not None:
                    keyword.append(val)
                    if sort:
                        try:
                            keyword.sort()
                        except TypeError:
                            keyword = (
                                sorted([el for el in keyword
                                        if not isinstance(el, str)]) +
                                [el for el in keyword
                                 if isinstance(el, str)]
                            )
            eccodes.codes_release(grib_it)
    return selections_dict


def print_available_values(fname, kw_list, sort=True):
    """Prints available values for each keyword of the given keywords list
    in a friendly format.

    kw_list: A list of the metadata keywords which the available values will be
    retrieved.
    """
    selections_dict = get_available_values(fname, kw_list, sort=sort)
    for key, values in selections_dict.items():
        print("{}: {}".format(key, values))


def _codes_get(var_id, key):
    try:
        val = eccodes.codes_get(var_id, key)
    except tuple(eccodes.ERROR_MAP.values()):
        val = None
    return val
